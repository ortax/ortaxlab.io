FROM ruby:2.3
MAINTAINER Gökhan Karabulut <gokhanettin@gmail.com>

# Copy the Gemfile into the image and run bundle install in a
# way that will be cached
WORKDIR /tmp
ADD Gemfile Gemfile
RUN bundle install

# Copy source
RUN mkdir -p /src
VOLUME ["/src"]
WORKDIR /src
ADD . /src

# Jekyll runs on port 4000 by default
EXPOSE 4000

# Run jekyll serve
CMD ["./jekyll_serve.sh"]
