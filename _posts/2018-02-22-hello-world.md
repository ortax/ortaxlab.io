---
title: Hello World Post
categories:
  - helloworld
author_staff_member: gokhan
show_comments: true
---

This is a "Hello World!" blog post. This post demonstrates how we write blog
pots on this platform. If you are not familiar with markdown format, please
read a [good markdown](https://guides.github.com/features/mastering-markdown/)
guide before writing a post.

Blog posts should also go through the [Merge
Request](https://docs.gitlab.com/ee/user/project/merge_requests/) process. So
you should be familiar with common software development concepts using git for
writing a blog post.
