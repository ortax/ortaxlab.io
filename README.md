# ORTAX HOMEPAGE

## Build

Install [Docker][1] and [Docker Compose][2]. Don't forget to continue with the
[Post-installation steps for Linux][3] after installing Docker. Once you are
done, building the homepage is as easy as running the following commands.

```bash
git clone https://gitlab.com/ortax/ortax.gitlab.io.git # Clone the repo
cd ortax.gitlab.io
docker-compose up # Build and start the docker image
```

The web page should be available at http://localhost:4000 when the docker image
is up and running.

## Development

Once you could successfully build the homepage locally, now you can start
developing it.

Always create a new branch and do your work on it. When you are happy with your
work, push your branch and send a Merge Request to the master branch.

```bash
git checkout -b <new-branch-name> # Create a new branch
docker-compose up # Build and start the docker image from your branch
# Edit the homepage as you wish. http://localhost:4000 will reflect your
# changes on the fly. You don't need to restart your docker image.
git push -u origin <new-branch-name> # Push your branch
# Now you can send a Merge Request to the master branch
```



[1]: https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#xenial-1604-and-newer
[2]: https://docs.docker.com/compose/install/#prerequisites
[3]: https://docs.docker.com/engine/installation/linux/linux-postinstall/
